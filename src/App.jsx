import Card from "./components/Card"
import Contact from "./components/Contact"
import Course from "./components/Course"
import Footer from "./components/Footer"
import Grand from "./components/Grand"
import Help from "./components/Help"
import Hero from "./components/Hero"
import Navbar from "./components/Navbar"
import Pcard from "./components/Pcard"
import { Link } from "react-scroll";
import Price from "./components/Price"

const App = () => {
  window.replainSettings = { id: '810ae5e4-db61-47a2-a091-f6968b2a9a74' };
(function(u){var s=document.createElement('script');s.async=true;s.src=u;
var x=document.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);
})('https://widget.replain.cc/dist/client.js');

  return (
    <div>
      <Navbar/>
      <Hero/>
      <Card/>
      <Course/>
      <Price/>
      <Grand/>
      <Pcard/>
      <Help/>
      <Contact/>


      <Footer/>
      <div id="mybutton" style={{ position: 'fixed', bottom: '10%', right: '10px', transform: 'translateY(-50%)' }}>
      <Link
                to="#contact"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
                className="btn bg-orange-600 text-white flex cursor-pointer items-center px-6 py-2 rounded-full gap-4"
              >
                <but className="">Royxatdan o`tish</but>
              </Link>
    </div>

    </div>
  )
}

export default App