const Pcard = () => {
  const course = [
    {
      id: 1,
      title: "DATA ANALYTICS",
      body: "Aniq tushunchalar va tahlillarga sho'ng'ing. Ma'lumotlardan mazmunli namunalarni olish, biznes qarorlarini qabul qilish imkoniyatlarini kengaytirish uchun ilg'or usullarni o'rganing. Bizning amaliy yondashuvimiz sizning ishga tayyor bo`lishingizni ta'minlaydi.",
      period: "10 oy",
    },
    {
      id: 2,
      title: "DATA ENGINEERING",
      body: "Ishonchli ma'lumotlar infratuzilmasini qurishni o'rganing. Data Engineering sohasida muvaffaqiyatli kar`yera qurish uchun ma'lumotlar tizimlarini loyihalash va joriy etish ko'nikmalariga ega bo'ling. Haqiqiy loyihalar sizning amaliy tajribangizni oshiradi.",
      period: "10 oy",
    },
  ];
  return (
    <div>
      <div className="container  mx-auto mt-4 sm:mt-8">
        <div className="uppercase text-center text-xl sm:text-2xl md:text-3xl text-blue-600 tracking-wider font-bold   ">
          MAAB Akademiyasida BI imkoniyatlarini oching!
        </div>
        <div className="px-2 sm:px-0 text-center text-sm sm:text-md md:text-lg mt-2 sm:mt-4 xl:w-[1200px] xl:mx-auto">
          Biz nafaqat nazariyani o`rgatamiz, balki o`quvchilarimizga qisqa vaqt
          ichida global miqyosda raqobatbardosh dasturchilar bo`lish
          imkoniyatini beradigan haqiqiy bilimlarni taqdim etamiz. Sizning
          kar`erangiz butkul o`zgarishi shu erdan boshlanadi
        </div>
        <div className=" flex gap-4 flex-wrap justify-center mt-8">
       

        {
            course.map((data)=>{
return (<div key={data.id} className="">
       <div className="card relative  flex flex-wrap sm:w-[550px] h-[350px] bg-stone-100 rounded-3xl ">
            <div className=" px-8 py-12">
              <div className=" text-blue-600 tracking-wider text-xl sm:text-2xl font-bold "> {data.title}</div>
              <div className="mt-4 text-sm sm:text-lg">
                {data.body}
              </div>
              
            </div>
            <div className="absolute mt-60 sm:mt-64 z-10 px-8">
                <div className="align-content: center;">  <strong>Davomiyligi: {data.period}</strong></div>
                {/* <div className=""></div> */}
            </div>

            <img src="/icons/pcard.png" alt="h-20" className="mt-60 absolute  " />
          </div>
</div>)
            })
        }
        </div>
      </div>
    </div>
  );
};

export default Pcard;
