const Card = () => {
  const cards = [
    {
      id: 1,
      title: "Shartnoma asosida ",
      body: "10 oylik kursni shartnoma asosida o'qishingiz mumkin",
    },
    {
      id: 2,
      title: "Shartli Grant",
      body: "Talant dasturi doirasida kursni bepul o'qib, kurs pulini shu soha bo`yicha ishga kirganingizdan so`ng, oyligingizning bir qismidan to'lashingiz mumkin.",
    },
    {
      id: 3,
      title: "100% Grant",
      body: "Imtihonlardan kerakli ballarni to`plash orqali kursni 100% bepul o'qishingiz mumkin.",
    },
  ];
  return (
    <div>
      <div className="container mx-auto flex flex-wrap lg:justify-between justify-center">
        {cards?.map((card) => {
          return <div key={card.id}>
             <div className="w-[350px] mx-auto sm:w-[420px] h-[360px]  bg-stone-100 flex flex-col rounded-3xl mt-4 xl:mt-0">
          <div className="flex justify-center mt-8 relative">
            <div className="absolute mt-9 text-2xl sm:text-3xl text-orange-600 text-wider font-semibold">
              {card.title}
            </div>
            <img src="/icons/cardt.png" alt="" className="w-[325px] sm:w-[380px] " width={"380px"} />
          </div>
          <div className=" flex-1 mx-auto text-center w-[280px] flex items-center justify-center ">
            <div className="">
              {card.body}
            </div>
          </div>
        </div>
          </div>;
        })}
       
      </div>
    </div>
  );
};

export default Card;
