import { useState } from "react";
import { Link } from "react-scroll";


const Navbar = () => {
  const [open, setOpen] = useState(true);

  return (
    <>
      <header className=" sticky px-2 sm:px-0">
        <div className="flex justify-between items-center container mx-auto pt-4 pb-3  ">
          <a href="https://academy.maab.uz/en/" className="">
            <img
              src="/icons/logo.png"
              alt="logo by MAAB"
              className="w-32 sm:w-40"
            />
          </a>
          
          <div className="sm:flex flex-col hidden">
            <div className="flex divide-x-2 gap-6 items-center justify-end  ">
              <div className="icons flex gap-4 items-center">
                <a
                  target="blank"
                  href="https://t.me/maabinnovation"
                  className="cursor-pointer "
                >
                  <img src="/icons/telegram.svg" alt="telegram" />
                </a>
                <a
                  href="https://www.instagram.com/maab.academy"
                  target="blank"
                  className="cursor-pointer "
                >
                  <img
                    src="/icons/instagram.svg"
                    alt="instagram"
                    className="sm:flex hidden"
                  />
                </a>
                <a
                  href="https://www.linkedin.com/maabinnavation"
                  target="blank"
                  className="cursor-pointer "
                >
                  <img
                    src="/icons/linkedin.svg"
                    alt="linkedin"
                    className="sm:flex hidden"
                  />
                </a>
                <a
                  href="https://www.youtube.com/@MAAB_Academy"
                  target="blank"
                  className="cursor-pointer "
                >
                  <img src="/icons/youtubef.svg" alt="youtube" />
                </a>
              </div>
              <div className="language flex pl-4 ">
                <select name="" id="">
                  <option value="uz">Uzbek</option>
                  <option value="ru">English</option>
                </select>
              </div>
            </div>
            <div className="flex items-center mt-4 gap-2">
              {/* <a
                href="tel:+998977839045"
                className="phone sm:flex hidden text-orange-600 font-semibold tracking-wide "
              >
                +998 97 783 90 45
              </a> */}
              <Link
                to="tel:+998781139669"
                spy={true}
                smooth={true}
                offset={-100}
                
                duration={500}
                className="btn bg-orange-600 text-white flex items-center px-6 py-2 rounded-full gap-4 cursor-pointer"
              >
                <but className="">+998781139669</but>
                <div className="">
                  <img src="/icons/call.svg" alt="call" />
                </div>
              </Link>
              {/* <Link
                to="#contact"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
                className="btn bg-orange-600 text-white flex items-center px-6 py-2 rounded-full gap-4"
              >
                <but className="">Royxatdan o`tish</but>
                <div className="">
                  <img src="/icons/call.svg" alt="call" />
                </div>
              </Link> */}
            </div>
          </div>
          <div className="flex sm:hidden absolute right-5">
            <div className="flex">
            <Link
                to="#contact"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
                className="btn   bg-orange-600 text-white flex items-center px-2 py-2 rounded-full gap-2"
              >
                <but className="">Ro`yxatdan o`tish</but>
                <div className="">
                  <img src="/icons/call.svg" alt="call" />
                </div>
              </Link>
              {open && (
                <button onClick={() => setOpen(false)}>
                  {" "}
                  <img src="/icons/bx-menu.svg" alt="" />
                </button>
              )}
              {!open && (
                <button onClick={() => setOpen(true)}>
                  {" "}
                  <img src="/icons/bx-x.svg" alt="" />
                </button>
              )}
            </div>
          </div>
        </div>
      </header>
      {!open && (
        <div className="transition ease-in-out delay-150  duration-1000   flex flex-col sm:hidden absolute w-full bg-stone-100 h-60 top-0 items-start px-2 py-4">
          <div className=" flex justify-between items-center w-full">
            <div className=""> <img src="/icons/logo.png" alt="logo" className="w-32" /> </div>
            <button onClick={() => setOpen(true)} className=" mr-3 -mt-1">
              {" "}
              <img src="/icons/bx-x.svg" alt="" />
            </button>
          </div>
          <div className="icons flex gap-4 items-center mx-auto  mt-4">
                <a
                  target="blank"
                  href="https://t.me/maabinnovation"
                  className="cursor-pointer "
                >
                  <img src="/icons/telegram.svg" alt="telegram" />
                </a>
                <a
                  href="https://www.instagram.com/maabinnovation"
                  target="blank"
                  className="cursor-pointer "
                >
                  <img
                    src="/icons/instagram.svg"
                    alt="instagram"
                    className="cursor-pointer"
                  />
                </a>
                <a
                  href="https://www.linkedin.com/maabinnavation"
                  target="blank"
                  className="cursor-pointer "
                >
                  <img
                    src="/icons/linkedin.svg"
                    alt="linkedin"
                    className="cursor-pointer"
                  />
                </a>
                <a
                  href="https://www.facebook.com/maabinnavation"
                  target="blank"
                  className="cursor-pointer "
                >
                  <img src="/icons/facebook.svg" alt="facebook" />
                </a>
              </div>
              <div className=" mx-auto mt-8">
              <a
                href="tel:+998977839045"
                className="phone   text-orange-600 font-semibold tracking-wide "
              >
                +998 97 783 90 45
              </a>
              </div>
              <div className="flex mx-auto mt-8 gap-2">
             
              <Link
                to="#contact"
                spy={true}
                smooth={true}
                offset={-100}
                duration={500}
                className="btn   bg-orange-600 text-white flex items-center px-6 py-2 rounded-full gap-4"
              >
                <but className="">Ro`yxatdan o`tish</but>
                <div className="">
                  <img src="/icons/call.svg" alt="call" />
                </div>
              </Link>
            </div>
        </div>
      )}
    </>
  );
};

export default Navbar;
