
const Footer = () => {
  return (
    <footer className="bg-[#0C081A]">
        <div className="flex flex-col container mx-auto">
<div className="maab flex bg-[#27242F] mt-10 py-10 px-10  rounded-3xl justify-between flex-wrap  ">
    <div className="flex flex-col gap-8 justify-center">
        <div className="logo"><img src="/icons/logof.png" alt="logo futer" width={"250px"} /></div>
        <a href="tel:+998977839045" className="phone tracking-wide font-semibold text-white ">+998 97 783 90 45</a>
        <div className="icons flex gap-3">
            <a href="#" className="flex justify-center  items-center py-2 border-stone-900 border  shadow-xl w-10 rounded-full "><img src="/icons/linkedinf.svg" alt="linkedinf" className=""/></a>
            <a href="/" className="flex justify-center  items-center py-2 border-stone-900 border  shadow-xl w-10 rounded-full "><img src="/icons/instagramf.svg" alt="instagramf" className=""/></a>
            <a href="/" className="flex justify-center  items-center py-2 border-stone-900 border  shadow-xl w-10 rounded-full "><img src="/icons/telgramf.svg" alt="telgramf" className=""/></a>
            <a href="/" className="flex justify-center  items-center py-2 border-stone-900 border  shadow-xl w-10 rounded-full "><img src="/icons/youtubef.svg" alt="youtubef" className=""/></a>
            <a href="/" className="flex justify-center  items-center py-2 border-stone-900 border  shadow-xl w-10 rounded-full "><img src="/icons/facebookf.svg" alt="facebookf" className=""/></a>
        </div>
    </div>
    <div className="flex flex-col gap-4 text-white mt-4 sm:mt-0">
        <div className="font-semibold cursor-pointer    tracking-wider    uppercase ">Kurslarimiz</div>
        <div className="tracking-wide cursor-pointer ">Data analytics</div>
        <div className="tracking-wide cursor-pointer">Data engineer</div>
        <div className="tracking-wide cursor-pointer">Cloud data engineering</div>
    </div>
    <div className="flex flex-col gap-4 text-white mt-4 sm:mt-0">
        <div className="font-semibold cursor-pointer    tracking-wider    uppercase ">Manyular</div>
        <div className="tracking-wide cursor-pointer ">Grantlar</div>
        <div className="tracking-wide cursor-pointer">Sertifikatlarimiz</div>
        <div className="tracking-wide cursor-pointer">Ustozlar</div>
    </div>
    <div className="flex flex-col gap-4 text-white mt-4 xl:mt-0">
        <div className="font-semibold cursor-pointer    tracking-wider    uppercase ">Biz bilan bog`laning</div>
        <div className="tracking-wide cursor-pointer flex gap-2">
           <img src="/icons/locationf.svg" alt="loacation MAAB" className="w-5"/>
            <div className=""><a href="https://academy.maab.uz/uz/contact/#contact-location">178/1, Maxtumquli Street, Tashkent City</a></div>
        </div>
        <div className="tracking-wide cursor-pointer flex gap-2">
        <img src="/icons/mailf.svg" alt="mail MAAB" className="w-5"/>
        <div className=""><a href="mailto:info@maab.uz"></a>info@maab.uz</div>
        </div>
        <div className="tracking-wide cursor-pointer flex gap-2">
        <img src="/icons/callf.svg" alt="phone MAAB" className="w-5"/>
            <div className="">+998 78 113 96 69</div>
        </div>
    </div>
</div>
<div className="copy flex justify-between items-center text-white mt-8">
    <div className="">&copy; {new Date().getFullYear()} MAAB INNOVATION</div>
    <div className="flex gap-2">
        <div className="">Barcha huquqlar himoyalangan</div>
        {/* <div className="">Dasturchilar: <a href="https://t.me/jasurbek_rakhmatov" target="blank" className="underline ">UniFS</a></div> */}
    </div>
</div>
        </div>
        .
    </footer>
  )
}

export default Footer