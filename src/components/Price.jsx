const Price = () => {
   const cards=[
    {
        id:1,
        title:"2 000 000+",
        body:"Dunyo bo`ylab bo`sh ish o`rinlari"
    },
    {
        id:2,
        title:"400$-2 000$+",
        body:"O`zbekistondagi maoshlar"
    },
    {
        id:3,
        title:"6 000$+",
        body:"AQSHdagi boshlang`ich maoshlar"
    },

   ]
  return (
    <div>
      <div className="mx-auto w-[350px] sm:w-full  bg-stone-100 container sm:mx-auto py-20 mt-4 sm:mt-8 rounded-3xl">
        <div className="text-center text-2xl md:text-3xl lg:text-4xl uppercase text-blue-500 font-bold ">
          BI mutaxassislarining daromadi qancha?
        </div>
        <div className="flex w-full justify-center gap-6 mt-8 flex-wrap ">
         
          {
            cards.map((card)=>{
                return(<div key={card.id}>
                 <div className="max-w-[350px] sm:h-[150px] relative">
            <div className=" absolute flex justify-center w-full mx-auto text-center h-full flex-col  ">
              <div className="text-3xl tracking-wider text-orange-600 font-semibold ">{card.title}</div>
              <div className="">{card.body}</div>
            </div>
            <img src="/icons/cardt.png" alt="" className="w-[320px] sm:w-full" />
          </div>
                </div>)
            })
          }

      
        
        </div>
      </div>
    </div>
  );
};

export default Price;
