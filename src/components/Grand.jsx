const Grand = () => {
  return (
    <div>
      <div className="container mx-auto relative mt-4 sm:mt-8 ">
        <div className="absolute mx-12 xl:py-12">
          <div className="text-xl md:text-2xl lg:text-4xl xl:text-5xl mt-5 md:mt-10  text-white tracking-wider">
            Grantni qo`lga kiriting!
          </div>
          <div className="flex items-center  lg:block">
            <div className="lg:w-[455px] text-sm mt-1 sm:mt-2 md:mt-3 xl:mt-4 text-stone-200 hidden sm:inline">
              MAAB Academyda biz eng yaxshi iqtidor egalariga oyiga 3+3 gacha
              grantlar taqdim etamiz. <span className="hidden sm:inline "> Agar siz o`zingizni iqtidorli deb
              hisoblasangiz va ingliz tilini mukammal bilsangiz, kirish
              imtihonida qatnashing! BI sayohatingizni biz bilan boshlash
              imkoniyatidan foydalaning!</span>
            </div>
            <div className="hidden md:flex">
              <button className="border mt-2 lg:mt-6 w-52 text-white px-8 py-2 rounded-full">
                Ro`yhatdan o`ting
              </button>
            </div>
          </div>
        </div>
        <img src="/icons/grand.png" alt="grand" className="w-[350px] mx-auto sm:w-full"/>
      </div>
    </div>
  );
};

export default Grand;
