const Course = () => {
  const cards = [
    {
      id: 1,
      title: "IT sohasiga qiziquvchilar uchun",
      body: "BI (Business Intelligence)ni o'rganmoqchi bo'lganlar uchun",
    },
    {
      id: 2,
      title:
        "Zamonaviy va eng talabgir kasb egasi bo'lishni xohlaganlar uchun.",
      body: "Hozirda BI mutaxassislariga bo'lgan talab kundan kunga ortib bormoqda va bu kursni o'rganish orqali yuqori daromadli kasb egasi bo'lishingiz mumkin.",
    },
    {
      id: 3,
      title: "Masofadan ishlashni xohlovchilar uchun",
      body: "BI mutaxassislari istalgan joydan masofaviy ishlash imkoniyatiga ega. O'zbekistonda turgan xolda Yevropa va Amerika loyihalarida ishlashingiz mumkin.",
    },
    {
      id: 4,
      title: "Mantiqiy va tanqidiy fikrlovchilar uchun ",
      body: "BI mutaxassislari ma`lumotlar bazasi bilan ishlaganliklari uchun ulardan mantiqiy va tanqidiy fikrlash talab qilinadi.",
    },
    {
      id: 5,
      title: "Ingliz tili darajasi B2 dan yuqori bo`lganlar uchun.",
      body: "Darslar asosan ingliz tilida o'tilganligi uchun o'quvchilardan ingliz tilini bilish talab qilinadi.",
    },
    {
      id: 6,
      title: "Tezroq daromadga chiqishni xohlovchilar uchun",
      body: "10 oyda kursni bitirib, BI mutaxassisiga aylanish orqali tezda daromadli kasb egasina aylanishiningiz mumkin.",
    },
  ];

  return (
    <div>
      <div className="container w-[350px] mx-auto mt-4 sm:mt-8 bg-blue-500 sm:w-full  py-20 p-10 rounded-3xl ">
        <h1 className="text-2xl sm:text-4xl  md:text-5xl lg:text-6xl text-center tracking-wider font-semibold uppercase text-white  ">
          Kurs kimlar uchun?
        </h1>
        <div className="flex flex-wrap text-center mx-auto justify-center gap-4 xl:w-[1200px] mt-8">
          {cards.map((card) => {
            return (
              <div key={card.id}>
                <div className="w-[320px] flex flex-col justify-center    sm:w-[388px] text-start px-6 text-xl h-[186px] rounded-xl bg-blue-400">
                    <div className=" text-white w-68 tracking-wider text-lg sm:text-xl">{card.title}</div>
                    <div className="pt-2  tracking-wide  text-xs sm:text-base  text-white  w-68">{card.body}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Course;
