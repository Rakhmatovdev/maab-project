import Dropdown from "./Dropdawn";

const Help = () => {
  const datas = [
    {
      id: 1,
      title:
        "“MAAB Academy” o’quv dasturida o’qish uchun qaysi fanlardan imtihon bo’lib o’tadi?",
      body: "O’quv dasturimizda o’qish imkoniyatini beruvchi dastlabki saralash sinov imtihoni mantiqiy va tanqidiy fikrlash, hamda ingliz tili bilim darajasini aniqlash bo’yicha bo’lib o’tadi.",
    },
    {
      id: 2,
      title:
        "“MAAB Academy”da qanday kurslar bor?",
      body: "Data Engineering, Data Analyst va Cloud Data Technologies yo'nalishida intensiv (chuqurlashtirilgan) kurslar mavjud.",
    },
    {
      id: 3,
      title:
        "Data Engineering yo’nalishida nima o’rgatiladi?",
      body: "Data Engineering kursi davomida – ma’lumotlar infratuzilmasini loyihalash, yaratish, boshqarish va avtomatlashtirish o’rgatiladi. ",
    },
    {
      id: 4,
      title:
        "Data Analyst yo’nalishida nima o’rgatiladi?",
      body: "Data Analytycs kursi davomida – biznes jarayonlarini optimallashtirish maqsadida asosli qaror qabul qilish uchun ma’lumotlar to’plamlaridan hisobotlar tayorlash o’rgatiladi. ",
    },
  ];
  const datas2 = [
    {
      id: 11,
      title: "“MAAB Academy”da o’qish uchun Ingliz tili talab etiladimi?",
      body: "Bizning kurslarimizda o’qiydigan talabalar Ingliz tili bilim darajasini aniqlash bo’yicha sertifikat borligi talab etilmaydi, ammo ingliz tili bilim darajasi kamida upper-intermediate (EILTS 5.5 yoki SEFR B2 ekvivalent) bilim darajada bo'lishi lozim. Nomzodlarning ingliz tili darajasi o’quv dasturimizda o’qish imkoniyatini beruvchi dastlabki saralash sinov imtihonida aniqlab olinadi.",
    },
    {
      id: 22,
      title: "Yosh chegarasi qanday?",
      body: "Markazimizda 16 yoshdan 35 yoshgacha yosh chegarasi mavjud. 15 yosh yoki 35 yoshdan yuqori bo'lgan o'quvchilar suhbat asosida o'qishlari mumkin bo'ladi.",
    },
    {
      id: 33,
      title: "“MAAB Academy”da Grant asosida tahsil olish imkoniyati mavjudmi?",
      body: "Dastlabki saralash sinov natijalari jadvali bo‘yicha tanqidiy-mantiqiy fikrlash testlaridan 90% dan kam bo‘lmagan hamda ingliz tili bilimini aniqlash imtihonidan kamida Advanced (IELTS 7 ball ekvivalenti) darajani ko‘rsatgan dastlabki 2 (ikki) ta ishtirokchi “Grant nomzodlari qisqa ro‘yxati”ga kiritiladi. 1 (bir) kalendar oy damovida o‘tkazilgan dastlabki saralash sinov imtihonlari yakunlari bo‘yicha shakllanadigan Grant nomzodlari qisqa ro‘yxati joy olgan ishtirokchilar orasidan Jamiyat tomonidan o‘tkaziladigan alohida imtihon/suhbat orqali grant g‘oliblari aniqlanadi. Grant g‘oliblari o‘quv dasturini to‘liq bepul o‘qish imkoniyatini qo‘lga kiritishadi ammo talaba o‘quv dasturi davomida Jamiyat tomonidan o‘zlashtirish darajasini aniqlash maqsadida tashkillashtiriladigan oraliq nazoratlaridan 90% dan kam bo‘lmagan ko‘rsatkich bilan o‘tishi shart hisoblanadi. Aks holda taqdim qilingan Grant bekor qilinadi hamda talaba kelgan joyidan shartnomaga muvofiq to‘lov qilishni boshlaydi. Shuningdek, Yoshlar ishlar agentligi tomonidan e'lon qilingan \"Kelajak kasblari\" grantiga topshirishingiz ham mumkin. Bunda Yoshlar ishlar agentligi tomonidan arizangiz tasdiqlansa, kurs to'lovlarining bir qismi agentlik tomonidan qoplab berilishi mumkin.",
    },
    {
      id: 4,
      title: "\"MAAB Academy\" ish bilan ta'minlaydimi?",
      body: "Jamiyat talabani ish bilan ta’minlashga yordam berishi mumkin bo‘lib, o‘z zimmasiga talabani ish bilan ta’minlash majburiyatini olmasdan imkoniyati doirasida o‘z ko‘magini amalga oshiradi. Ko‘rsatilishi mumkin bo‘lgan bu tartibdagi yordam uchun talabadan haq olinmaydi. Bugungi kungacha bizning bitiruvchilarimizning 99 foizi ish bilan ta’minlangan.",
    },
  ];
  return (
    <div className="bg-stone-100">
      <div className="container mx-auto mt-8 py-20">
        <div className="text-2xl sm:text-5xl uppercase font-bold tracking-wide text-center ">
          Ko`p so`ralgan savollar
        </div>
        <div className="text-sm sm:text-lg text-center lg:w-[850px] mx-auto sm:mt-4 mt-2">
        </div>
        <div className="drop flex flex-col sm:flex-row   justify-center gap-4 mx-auto">
          <div className="flex flex-col mx-auto sm:mx-2">
            {datas.map((data) => {
              return (
                <>
                  <Dropdown key={data.id} body={data.body} title={data.title} />
                </>
              );
            })}
          </div>
          <div className="flex flex-col mx-auto sm:mx-2">
            {datas2.map((data) => {
              return (
                <>
                  <Dropdown key={data.id} body={data.body} title={data.title} />
                </>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Help;
