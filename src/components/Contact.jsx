import axios from "axios";
import { useForm } from "react-hook-form"
import toast, { Toaster } from "react-hot-toast";

const Contact = () => {
    const {
        register,
        handleSubmit,
        reset,
      } = useForm()
    
      const onSubmit = async (data) => {

          console.log(data)
          toast.success('Ma`lumotlaringiz uchun raxmat!')
       await axios.post('https://flaskapitelegram.onrender.com/send_data', data)
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
          reset()
      }
    
    
    
  return (
    <div>
      <div className="container mx-auto px-10 py-16 my-4 sm:my-8 bg-stone-100 rounded-xl ">
        <div className="flex flex-col gap-8 sm:flex-row ">
          <div className="flex-1">
            <div className="tracking-wider font-semibold text-xl sm:text-2xl xl:w-[350px]  ">
              MAAB Academy bilan BI kelajagingizni oching! Hozir ro`yxatdan
              o`ting!
            </div>
            <div className="text-sm sm:text-md mt-2">
              Kirish imtihonimizni topshiring va Grant yutib olish imkoniyatiga
              ega bo`ling
            </div>
          </div>
          <div  id="#contact" className="sm:w-[60%] mt-4 sm:mt-0 bg-white rounded-xl px-5 py-5">
            <div className="tracking-wider font-semibold text-md sm:text-lg   ">
              Kursga yozilish
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="flex gap-2 mt-4 md:flex-row flex-col">
                <label>
                  <span className="tracking-wide font-semibold ">Ismingiz</span>
                  <div className="bg-stone-100 px-3  py-2 xl:w-[335px] rounded-lg mt-1">
                    {" "}
                    <input
                      type="text"
                      className="outline-none w-full bg-inherit "
                      placeholder="Full name..."
                      {...register("full_name")}
                      required
                    />
                  </div>
                </label>
                <label>
                  <span className="tracking-wide font-semibold">
                    Telefon raqamingiz
                  </span>
                  <div className="bg-stone-100 px-3  py-2 xl:w-[335px] rounded-lg mt-1">
                    {" "}
                    <input
                      type="phone"
                      className="outline-none w-full bg-inherit "
                      placeholder="+998"
                      {...register("phone")}
                      required
                    />
                  </div>
                </label>
              </div>
              {/*  */}
              <div className="flex gap-2 mt-2 md:mt-4 md:flex-row flex-col">
                <label>
                  <span className="tracking-wide font-semibold ">
                    Ingliz tili darajangiz
                  </span>
                  <div className="bg-stone-100 px-3  py-2 xl:w-[335px] rounded-lg mt-1">
                    {" "}
                    <input
                      type="text"
                      className="outline-none w-full bg-inherit "
                      placeholder="English score..."
                      {...register("english_score")}
                      required
                    />
                  </div>
                </label>
                <label>
                  <span className="tracking-wide font-semibold ">
                    Mantiqiy bilim darajangiz
                  </span>
                  <div className="bg-stone-100 px-3  py-2 xl:w-[335px] rounded-lg mt-1">
                    <select className="outline-none w-full bg-inherit ">
                      <option defaultValue={null}>Bilim darajangiz</option>
                      <option value="a">Beginner</option>
                      <option value="b">Elementry</option>
                      <option value="c">Intermident</option>
                    </select>
                  </div>
                </label>
              </div>
              <div className="flex flex-col  sm:flex-row justify-between">
                <label className="flex mt-2 sm:mt-6 gap-2">
                  <div className="">
                    <input type="checkbox"  />
                  </div>
                  <span className=" xl:w-[450px] ">
                    & Arizani topshirish orqali siz shaxsiy ma`lumotlaringizni “{" "}
                    <a
                      href="/"
                      className="underline text-blue-500 hover:text-fuchsia-500 "
                    >
                      {" "}
                      Maxfiylik siyosati
                    </a>
                    ”ga muvofiq qayta ishlashga rozilik bildirasiz
                  </span>
                </label>
                <label>
                  <div className=" mt-2 sm:mt-6 bg-blue-600 px-4 py-2 w-52 text-white rounded-full text-center">
                    <button type="submit">Arizani jo`natish -&gt;</button>
                  </div>
                </label>
              </div>
            </form>
          </div>
        </div>
      </div>
      <Toaster
  position="top-right"
  reverseOrder={false}
/>


    </div>
  );
};

export default Contact;
